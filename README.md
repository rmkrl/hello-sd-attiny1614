# Hello SD ATtiny1614

Basic SD card test board with ATtiny1614. Use RML files in [rml](rml) directory to mill 
with SRM-20. File names include milling bit diameter. It is a single sided board and should 
cause no much trouble. 

> WARNING: Make sure you use single-sided copper clad. 

There is no need to spoil double-sided material. Board dimensions are **35x25mm**.

## BOM

| Ref | Name | Value | Package | Count |
|--- |--- |--- |--- |--- |
| R1, R2 | Resistor | 100 OHM | 1206 | 2 |
| D1, D2 | LED | Any color | 1206 | 2 |
| R3 | Resistor | 0 OHHM | 1206 | 1 |
| C1 | Capacitor | 1 uF | 1206 | 1 |
| U1 | Microcontroller | ATtiny1614 | SOIC-14 | 1 |
| J3 | SD Card Socket | Micro SD | Amphenol | 1 |
| J2 | Pin Header | 2 pos UPDI | SMD | 1 |
| J1 | Pin Header | 6 pos FTDI | SMD | 1 | 

![Schematic](doc/hello-sd-attiny1614-sch.png)

![Layout](doc/hello-sd-attiny1614-pcb.png)

![ATtiny1614 Pinout](doc/ATtiny_x14.gif)
