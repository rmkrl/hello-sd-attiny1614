EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L fab:Micro_SD_Socket J3
U 1 1 5F4D5177
P 8325 3000
F 0 "J3" H 8275 3717 50  0000 C CNN
F 1 "Micro_SD_Socket" H 8275 3626 50  0000 C CNN
F 2 "fab:MicroSD_Amphenol_114-00841-68" H 8325 3000 50  0001 C CNN
F 3 "http://www.ti.com/lit/ds/symlink/opa188.pdf" H 8325 3000 50  0001 C CNN
	1    8325 3000
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x06 J1
U 1 1 5F4D6BA1
P 7700 4900
F 0 "J1" H 7780 4892 50  0000 L CNN
F 1 "FTDI" H 7780 4801 50  0000 L CNN
F 2 "fab:fab-1X06SMD" H 7700 4900 50  0001 C CNN
F 3 "~" H 7700 4900 50  0001 C CNN
	1    7700 4900
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x02 J2
U 1 1 5F4D8A49
P 7800 1525
F 0 "J2" H 7880 1517 50  0000 L CNN
F 1 "UPDI" H 7880 1426 50  0000 L CNN
F 2 "fab:fab-1X02SMD" H 7800 1525 50  0001 C CNN
F 3 "~" H 7800 1525 50  0001 C CNN
	1    7800 1525
	1    0    0    -1  
$EndComp
$Comp
L fab:LED D2
U 1 1 5F4D9D0C
P 4625 4725
F 0 "D2" H 4618 4941 50  0000 C CNN
F 1 "LED_PRG" H 4618 4850 50  0000 C CNN
F 2 "fab:LED_1206" H 4625 4725 50  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS-22-98-0002/LTST-C150CKT.pdf" H 4625 4725 50  0001 C CNN
	1    4625 4725
	1    0    0    -1  
$EndComp
$Comp
L fab:R R1
U 1 1 5F4DA5A4
P 5100 4725
F 0 "R1" V 4893 4725 50  0000 C CNN
F 1 "100" V 4984 4725 50  0000 C CNN
F 2 "fab:R_1206" V 5030 4725 50  0001 C CNN
F 3 "~" H 5100 4725 50  0001 C CNN
	1    5100 4725
	0    1    1    0   
$EndComp
$Comp
L fab:LED D1
U 1 1 5F4E00D4
P 4600 5550
F 0 "D1" H 4593 5766 50  0000 C CNN
F 1 "LED_PWR" H 4593 5675 50  0000 C CNN
F 2 "fab:LED_1206" H 4600 5550 50  0001 C CNN
F 3 "https://optoelectronics.liteon.com/upload/download/DS-22-98-0002/LTST-C150CKT.pdf" H 4600 5550 50  0001 C CNN
	1    4600 5550
	1    0    0    -1  
$EndComp
$Comp
L fab:R R2
U 1 1 5F4E00DA
P 5125 5550
F 0 "R2" V 4918 5550 50  0000 C CNN
F 1 "100" V 5009 5550 50  0000 C CNN
F 2 "fab:R_1206" V 5055 5550 50  0001 C CNN
F 3 "~" H 5125 5550 50  0001 C CNN
	1    5125 5550
	0    1    1    0   
$EndComp
$Comp
L fab:C C1
U 1 1 5F4E052C
P 3575 3150
F 0 "C1" H 3690 3196 50  0000 L CNN
F 1 "1uF" H 3690 3105 50  0000 L CNN
F 2 "fab:fab-C1206" H 3613 3000 50  0001 C CNN
F 3 "" H 3575 3150 50  0001 C CNN
	1    3575 3150
	1    0    0    -1  
$EndComp
Wire Wire Line
	7500 4700 7350 4700
Wire Wire Line
	7500 4900 7350 4900
Wire Wire Line
	7500 5000 7050 5000
Wire Wire Line
	7500 5100 7050 5100
Text Label 7050 4700 0    50   ~ 0
GND
Text Label 7050 4900 0    50   ~ 0
VCC
Text Label 7050 5000 0    50   ~ 0
TX
Text Label 7050 5100 0    50   ~ 0
RX
NoConn ~ 7500 4800
NoConn ~ 7500 5200
Wire Wire Line
	7600 1525 6875 1525
Wire Wire Line
	7600 1625 6875 1625
Text Label 6875 1525 0    50   ~ 0
UPDI
Text Label 6875 1625 0    50   ~ 0
GND
$Comp
L fab:Microcontroller_ATtiny1614-SSFR U1
U 1 1 5F4D4361
P 4850 3125
F 0 "U1" H 4850 4006 50  0000 C CNN
F 1 "Microcontroller_ATtiny1614-SSFR" H 4850 3915 50  0000 C CNN
F 2 "fab:SOIC-14_3.9x8.7mm_P1.27mm" H 4850 3125 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATtiny1614-16-17-DataSheet-DS40002204A.pdf" H 4850 3125 50  0001 C CNN
	1    4850 3125
	1    0    0    -1  
$EndComp
Wire Wire Line
	3575 3000 3575 2025
Wire Wire Line
	3575 2025 4850 2025
Wire Wire Line
	4850 2025 4850 2425
Wire Wire Line
	3575 3300 3575 4125
Wire Wire Line
	3575 4125 4850 4125
Wire Wire Line
	4850 4125 4850 3825
NoConn ~ 7425 2700
NoConn ~ 7425 3400
Wire Wire Line
	7425 3100 6875 3100
Text Label 6875 3100 0    50   ~ 0
SCK
Wire Wire Line
	5450 3125 6000 3125
Text Label 6000 3125 2    50   ~ 0
SCK
Wire Wire Line
	7425 3000 6875 3000
Text Label 6875 3000 0    50   ~ 0
VCC
Wire Wire Line
	7425 2800 6875 2800
Text Label 6875 2800 0    50   ~ 0
SS
Wire Wire Line
	5450 3225 6000 3225
Text Label 6000 3225 2    50   ~ 0
SS
Wire Wire Line
	7425 2900 6875 2900
Text Label 6875 2900 0    50   ~ 0
MOSI
Text Label 6225 3200 0    50   ~ 0
GND
Wire Wire Line
	7425 3300 6875 3300
Text Label 6875 3300 0    50   ~ 0
MISO
Wire Wire Line
	5450 3025 6000 3025
Text Label 6000 3025 2    50   ~ 0
MISO
Wire Wire Line
	5450 2925 6000 2925
Text Label 6000 2925 2    50   ~ 0
MOSI
Wire Wire Line
	5450 3525 6000 3525
Text Label 6000 3525 2    50   ~ 0
LED
NoConn ~ 5450 3325
NoConn ~ 5450 3425
Wire Wire Line
	5450 2825 6000 2825
Text Label 6000 2825 2    50   ~ 0
UPDI
Wire Wire Line
	4250 3025 3900 3025
Wire Wire Line
	4250 3125 3900 3125
NoConn ~ 4250 2825
NoConn ~ 4250 2925
Wire Wire Line
	4775 4725 4950 4725
Wire Wire Line
	4750 5550 4975 5550
Wire Wire Line
	4475 4725 4075 4725
Wire Wire Line
	5250 4725 5775 4725
Wire Wire Line
	4450 5550 4075 5550
Text Label 4075 4725 0    50   ~ 0
GND
Text Label 4075 5550 0    50   ~ 0
GND
Text Label 5775 4725 2    50   ~ 0
LED
Text Label 5800 5550 2    50   ~ 0
VCC
$Comp
L power:PWR_FLAG #FLG02
U 1 1 5F4F0FBF
P 7350 4900
F 0 "#FLG02" H 7350 4975 50  0001 C CNN
F 1 "PWR_FLAG" H 7350 5073 50  0000 C CNN
F 2 "" H 7350 4900 50  0001 C CNN
F 3 "~" H 7350 4900 50  0001 C CNN
	1    7350 4900
	1    0    0    -1  
$EndComp
Connection ~ 7350 4900
$Comp
L power:PWR_FLAG #FLG01
U 1 1 5F4F14BA
P 7350 4700
F 0 "#FLG01" H 7350 4775 50  0001 C CNN
F 1 "PWR_FLAG" H 7350 4873 50  0000 C CNN
F 2 "" H 7350 4700 50  0001 C CNN
F 3 "~" H 7350 4700 50  0001 C CNN
	1    7350 4700
	1    0    0    -1  
$EndComp
Connection ~ 7350 4700
Wire Wire Line
	7350 4700 7050 4700
Wire Wire Line
	7350 4900 7050 4900
Wire Wire Line
	5275 5550 5800 5550
Text Label 3575 2025 0    50   ~ 0
VCC
Text Label 3575 4125 0    50   ~ 0
GND
Wire Wire Line
	9125 3600 9875 3600
Text Label 9875 3600 2    50   ~ 0
GND
$Comp
L fab:R R3
U 1 1 5F50BD99
P 6725 3200
F 0 "R3" V 6518 3200 50  0000 C CNN
F 1 "0" V 6609 3200 50  0000 C CNN
F 2 "fab:R_1206" V 6655 3200 50  0001 C CNN
F 3 "~" H 6725 3200 50  0001 C CNN
	1    6725 3200
	0    1    1    0   
$EndComp
Wire Wire Line
	6575 3200 6225 3200
Text Label 3900 3025 0    50   ~ 0
RX
Text Label 3900 3125 0    50   ~ 0
TX
Wire Wire Line
	6875 3200 7175 3200
$Comp
L power:PWR_FLAG #FLG0101
U 1 1 5F59E549
P 7175 3200
F 0 "#FLG0101" H 7175 3275 50  0001 C CNN
F 1 "PWR_FLAG" H 7175 3373 50  0000 C CNN
F 2 "" H 7175 3200 50  0001 C CNN
F 3 "~" H 7175 3200 50  0001 C CNN
	1    7175 3200
	1    0    0    -1  
$EndComp
Connection ~ 7175 3200
Wire Wire Line
	7175 3200 7425 3200
$EndSCHEMATC
